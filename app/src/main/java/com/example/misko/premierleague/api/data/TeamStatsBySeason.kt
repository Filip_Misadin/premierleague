package com.example.misko.premierleague.api.data

data class TeamStatsBySeason(
    val api: Api
)

data class Api(
    val results: Int,
    val statistics: TeamStatistics
)

data class TeamStatistics(
    val goals: Goals,
    val goalsAvg: GoalsAvg,
    val matchs: Matchs
)

data class Goals(
    val goalsAgainst: StatsByStadium,
    val goalsFor: StatsByStadium
)

data class GoalsAvg(
    val goalsAgainst: StatsByStadiumString,
    val goalsFor: StatsByStadiumString
)

data class Matchs(
    val draws: StatsByStadium,
    val loses: StatsByStadium,
    val matchsPlayed: StatsByStadium,
    val wins: StatsByStadium
)

//divided on int and string for math functions
data class StatsByStadium(
    val away: Int,
    val home: Int,
    val total: Int
)

data class StatsByStadiumString(
    val away: String,
    val home: String,
    val total: String
)
