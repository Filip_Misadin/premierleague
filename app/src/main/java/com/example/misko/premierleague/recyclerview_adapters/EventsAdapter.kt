package com.example.misko.premierleague.recyclerview_adapters

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import com.example.misko.premierleague.activities.data.Event
import com.example.misko.premierleague.R
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.view_event.view.*

class EventsAdapter(private var viewlist: List<Event>): androidx.recyclerview.widget.RecyclerView.Adapter<CustomViewHolder>() {

    // numberOfItems
    override fun getItemCount(): Int {
        return viewlist.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val cellForRow = layoutInflater.inflate(R.layout.view_event, parent, false)
        return CustomViewHolder(cellForRow)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: CustomViewHolder, position: Int) {
        val event = viewlist[position]
        holder.view.txtGoal.text = ""
        holder.view.txtPlayer.text = event.player
        holder.view.txtTeamName.text = event.player
        holder.view.txtMinute.text = event.elapsed.toString() + "'"

        when {
            event.detail == "Penalty" -> loadImage(R.drawable.goal, holder.view.imgEventIcon)
            event.detail == "Normal Goal" -> loadImage(R.drawable.goal, holder.view.imgEventIcon)
            event.detail == "Yellow Card" -> loadImage(R.drawable.yellow_card, holder.view.imgEventIcon)
            event.detail == "Red Card" -> loadImage(R.drawable.red_card, holder.view.imgEventIcon)
            else -> {
                loadImage(R.drawable.sub_icon, holder.view.imgEventIcon)

                holder.view.txtTeamName.text = event.detail
            }
        }
    }

    private fun loadImage(drawable: Int, view: ImageView){
        Picasso.get().load(drawable).fit().into(view)
    }
}