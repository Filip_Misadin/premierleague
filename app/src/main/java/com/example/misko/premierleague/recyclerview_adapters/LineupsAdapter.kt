package com.example.misko.premierleague.recyclerview_adapters

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import com.example.misko.premierleague.activities.MatchActivity
import com.example.misko.premierleague.R
import com.example.misko.premierleague.api.data.Lineup
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.view_lineup.view.*

class LineupsAdapter(private var viewlist: List<Lineup>): androidx.recyclerview.widget.RecyclerView.Adapter<CustomViewHolder>() {

    // numberOfItems
    override fun getItemCount(): Int {
        return viewlist.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val cellForRow = layoutInflater.inflate(R.layout.view_lineup, parent, false)
        return CustomViewHolder(cellForRow)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: CustomViewHolder, position: Int) {
        val lineup = viewlist[position]
        if(position == 0) {
            Picasso.get().load(MatchActivity.match!!.homeTeam.logo).fit().into(holder.view.imgTeamIcon)
            holder.view.txtTeamName.text = MatchActivity.match!!.homeTeam.name
        } else {
            Picasso.get().load(MatchActivity.match!!.awayTeam.logo).fit().into(holder.view.imgTeamIcon)
            holder.view.txtTeamName.text = MatchActivity.match!!.awayTeam.name
        }

        holder.view.txtCoachName.text = lineup.coach
        holder.view.txtFormation.text = lineup.formation

        holder.view.recyclerViewStartXI.layoutManager =
            androidx.recyclerview.widget.LinearLayoutManager(holder.view.context)
        holder.view.recyclerViewStartXI.adapter = PlayerAdapter(lineup.startXI)

        holder.view.recyclerViewSubs.layoutManager =
            androidx.recyclerview.widget.LinearLayoutManager(holder.view.context)
        holder.view.recyclerViewSubs.adapter = SubstituteAdapter(lineup.substitutes)
    }

}