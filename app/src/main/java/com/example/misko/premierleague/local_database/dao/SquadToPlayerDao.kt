package com.example.misko.premierleague.local_database.dao

import androidx.room.*
import com.example.misko.premierleague.local_database.entities.SquadToPlayer

@Dao
interface SquadToPlayerDao{
    @Query("SELECT * FROM squadToPlayer")
    fun getAll(): List<SquadToPlayer>

    @Query("SELECT * FROM squadToPlayer WHERE squadId LIKE :squadId")
    fun findById(squadId: Long): SquadToPlayer?

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(vararg squadToPlayers: SquadToPlayer)

    @Delete
    fun delete(squadToPlayer: SquadToPlayer)

    @Update
    fun updateSquadToPlayer(vararg squadToPlayer: SquadToPlayer)
}