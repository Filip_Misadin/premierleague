package com.example.misko.premierleague.fragments

import android.annotation.SuppressLint
import android.content.Context
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.example.misko.premierleague.*
import com.example.misko.premierleague.activities.MatchActivity
import com.example.misko.premierleague.activities.data.Event
import com.example.misko.premierleague.activities.data.Match

import com.example.misko.premierleague.recyclerview_adapters.EventsAdapter
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_match_details.view.*
import java.text.SimpleDateFormat
import java.util.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [DetailsFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [DetailsFragment.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class DetailsFragment : androidx.fragment.app.Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    private var listener: OnFragmentInteractionListener? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    @SuppressLint("SetTextI18n")
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_match_details, container, false)

        val match = MatchActivity.match
        if(match != null) {
            val homeTeamName = reduceTeamName(match.homeTeam.name)
            val awayTeamName = reduceTeamName(match.awayTeam.name)

            val date = formatDate(match.date)

            loadTeamIcon(match.homeTeam.logo, view.imgHomeIcon)
            loadTeamIcon(match.awayTeam.logo, view.imgAwayIcon)

            showData(view, homeTeamName, awayTeamName, date, match)

            setRecyclerView(view, match.events)
        }

        return view
    }

    private fun setRecyclerView(view: View, events: List<Event>){
        view.recyclerViewEvents.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(activity)
        view.recyclerViewEvents.adapter = EventsAdapter(events)
    }

    private fun showData(view: View, homeTeamName: String, awayTeamName: String, date: String, match: Match){
        view.txtHomeTeam.text = homeTeamName
        view.txtAwayTeam.text = awayTeamName
        view.txtMatchDate.text = date
        view.txtFinalScore.text = getString(R.string.score_text, match.goalsHome, match.goalsAway)
        view.txtHalfTimeScore.text = match.halfTimeScore
        if(match.odds.isNotEmpty()){
            //0 - Home, 1 - Draw, 2 - Away
            view.txtOddsHome.text = getOddsPercentageString(match.odds[0])
            view.txtOddsDraw.text = getOddsPercentageString(match.odds[1])
            view.txtOddsAway.text = getOddsPercentageString(match.odds[2])
        }
        view.txtHomeTeam2.text = match.homeTeam.name
        view.txtAwayTeam2.text = match.awayTeam.name
    }

    private fun getOddsPercentageString(odd: Double): String{
        val oddPercentage = (odd * 100).toInt()
        return getString(R.string.percentage_text, oddPercentage)
    }

    private fun loadTeamIcon(logo: String, view: ImageView){
        Picasso.get().load(logo).fit().into(view)
    }

    private fun reduceTeamName(teamName: String): String{
        if(teamName.contains("Manchester"))
            return teamName.replace("Manchester", "Man.")
        return teamName
    }

    private fun formatDate(date: String): String{
        val parser = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.getDefault())
        val formatter = SimpleDateFormat("dd.MM.yyyy", Locale.getDefault())
        return formatter.format(parser.parse(date))
    }

    // TODO: Rename method, update argument and hook method into UI event
    fun onButtonPressed(uri: Uri) {
        listener?.onFragmentInteraction(uri)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments]
     * (http://developer.android.com/training/basics/fragments/communicating.html)
     * for more information.
     */
    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onFragmentInteraction(uri: Uri)
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment DetailsFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
                DetailsFragment().apply {
                    arguments = Bundle().apply {
                        putString(ARG_PARAM1, param1)
                        putString(ARG_PARAM2, param2)
                    }
                }
    }
}
