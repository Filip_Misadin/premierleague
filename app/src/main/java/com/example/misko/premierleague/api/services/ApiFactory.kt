package com.example.misko.premierleague.api.services

import com.example.misko.premierleague.AppConstants

object ApiFactory{
    val soccerApi : SoccerApi = RetrofitFactory.retrofit(
        AppConstants.API_BASE_URL
    )
            .create(SoccerApi::class.java)
}