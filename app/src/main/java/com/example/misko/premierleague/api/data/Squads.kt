package com.example.misko.premierleague.api.data

data class Squads(
        val api: ApiS = ApiS(
                emptyList(),
                emptyList(),
                -1
        )
)

data class ApiS(
        val coachs: List<String>,
        val players: List<Player>,
        val results: Int
)