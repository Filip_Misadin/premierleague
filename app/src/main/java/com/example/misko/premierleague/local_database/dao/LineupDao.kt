package com.example.misko.premierleague.local_database.dao

import androidx.room.*
import com.example.misko.premierleague.local_database.entities.Lineup

@Dao
interface LineupDao{
    @Query("SELECT * FROM Lineup")
    fun getAll(): List<Lineup>

    @Query("SELECT * FROM Lineup WHERE id LIKE :id")
    fun findById(id: Long?): Lineup

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(lineup: Lineup): Long

    @Delete
    fun delete(lineup: Lineup)

    @Update
    fun updateLineup(vararg lineup: Lineup)
}