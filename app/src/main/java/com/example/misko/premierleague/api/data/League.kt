package com.example.misko.premierleague.api.data

import com.google.gson.annotations.SerializedName

data class League(
        val api: ApiL
)

data class ApiL(
        val leagues: List<LeagueDetails>,
        val results: Int
)

data class LeagueDetails(
        val country: String,
        @SerializedName("country_code")
        val countryCode: String,
        val flag: String,
        @SerializedName("is_current")
        val isCurrent: Int,
        @SerializedName("league_id")
        val leagueId: Long,
        val logo: String,
        val name: String,
        val season: Int,
        @SerializedName("season_end")
        val seasonEnd: String,
        @SerializedName("season_start")
        val seasonStart: String,
        val standings: Int
)