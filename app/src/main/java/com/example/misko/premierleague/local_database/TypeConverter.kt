package com.example.misko.premierleague.local_database

import androidx.room.TypeConverter
import java.sql.Date

class TypeConverter {
    @TypeConverter
    fun fromTimestamp(value: Long?): Date? {
        return when (value) {
            null -> null
            else -> Date(value)
        }
    }

    @TypeConverter
    fun dateToTimestamp(date: Date?): Long? {
        return date?.time
    }
}