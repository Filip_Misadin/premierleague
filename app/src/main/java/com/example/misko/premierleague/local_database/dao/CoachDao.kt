package com.example.misko.premierleague.local_database.dao

import androidx.room.*
import com.example.misko.premierleague.local_database.entities.Coach

@Dao
interface CoachDao{
    @Query("SELECT * FROM coach")
    fun getAll(): List<Coach>

    @Query("SELECT * FROM coach WHERE id LIKE :id")
    fun findById(id: Int): Coach?

    @Query("SELECT * FROM coach WHERE teamName LIKE :teamName")
    fun getAllInTeam(teamName: String): List<Coach>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(vararg coach: Coach)

    @Delete
    fun delete(coach: Coach)

    @Update
    fun updateCoach(vararg coach: Coach)
}