package com.example.misko.premierleague.api.data

import java.io.Serializable

data class Lineups(
        val api: ApiLU
)

data class ApiLU(
        val lineUps: Map<String, Lineup>,
        val results: Int
)

data class Lineup(
        val coach: String,
        val formation: String,
        val startXI: MutableList<Player>,
        val substitutes: MutableList<Player>
) : Serializable