package com.example.misko.premierleague.api.data


data class HeadToHead(
        val api: ApiHTH
)

data class ApiHTH(
        val fixtures: List<Fixture>,
        val results: Int
)