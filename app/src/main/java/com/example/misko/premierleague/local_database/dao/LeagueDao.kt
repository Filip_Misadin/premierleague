package com.example.misko.premierleague.local_database.dao

import androidx.room.*
import com.example.misko.premierleague.local_database.entities.League

@Dao
interface LeagueDao{
    @Query("SELECT * FROM league")
    fun getAll(): List<League>

    @Query("SELECT * FROM league WHERE id LIKE :id")
    fun findById(id: Long): League?

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(vararg league: League)

    @Delete
    fun delete(league: League)

    @Update
    fun updateLeague(vararg league: League)
}