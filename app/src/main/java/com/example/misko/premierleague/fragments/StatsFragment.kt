package com.example.misko.premierleague.fragments

import android.content.Context
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.example.misko.premierleague.activities.data.Match
import com.example.misko.premierleague.activities.MatchActivity
import com.example.misko.premierleague.R
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_stats.view.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [StatsFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [StatsFragment.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class StatsFragment : androidx.fragment.app.Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    private var listener: OnFragmentInteractionListener? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_stats, container, false)

        val match = MatchActivity.match
        if(match != null){
            val teamNameHome = reduceTeamName(match.homeTeam.name)
            val teamNameAway = reduceTeamName(match.awayTeam.name)

            showTeamNames(view, teamNameHome, teamNameAway)

            loadTeamIcon(match.homeTeam.logo, view.imgHomeIcon)
            loadTeamIcon(match.awayTeam.logo, view.imgAwayIcon)


            if(match.ballPossesion.isNotEmpty()){
                showData(view, match)
            }
        }

        return view
    }

    private fun showData(view: View, match: Match){
        view.txtBallPossesionHome.text = match.ballPossesion[0]
        view.txtBallPossesionAway.text = match.ballPossesion[1]
        view.txtShotsOffGoalHome.text = match.shotsOffGoal[0].toString()
        view.txtShotsOffGoalAway.text = match.shotsOffGoal[1].toString()
        view.txtShotsOnGoalHome.text = match.shotsOnGoal[0].toString()
        view.txtShotsOnGoalAway.text = match.shotsOnGoal[1].toString()
        view.txtTotalPassesHome.text = match.totalPasses[0].toString()
        view.txtTotalPassesAway.text = match.totalPasses[1].toString()
        view.txtPassesAccurateHome.text = match.passesAccurate[0].toString()
        view.txtPassesAccurateAway.text = match.passesAccurate[1].toString()
        view.txtFoulsHome.text = match.fouls[0].toString()
        view.txtFoulsAway.text = match.fouls[1].toString()
        view.txtCornerKicksHome.text = match.cornerKicks[0].toString()
        view.txtCornerKicksAway.text = match.cornerKicks[1].toString()
        view.txtOffsidesHome.text = match.offsides[0].toString()
        view.txtOffsidesAway.text = match.offsides[1].toString()
        view.txtYellowCardsHome.text = match.yellowCards[0]
        view.txtYellowCardsAway.text = match.yellowCards[1]
        view.txtRedCardsHome.text = match.redCards[0] ?: "0"
        view.txtRedCardsAway.text = match.redCards[1] ?: "0"
    }

    private fun showTeamNames(view: View, home: String, away: String){
        view.txtHomeTeam.text = home
        view.txtAwayTeam.text = away
    }

    private fun loadTeamIcon(logo: String, view: ImageView){
        Picasso.get().load(logo).fit().into(view)
    }

    private fun reduceTeamName(teamName: String): String{
        if(teamName.contains("Manchester"))
            return teamName.replace("Manchester", "Man.")
        return teamName
    }

    // TODO: Rename method, update argument and hook method into UI event
    fun onButtonPressed(uri: Uri) {
        listener?.onFragmentInteraction(uri)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments]
     * (http://developer.android.com/training/basics/fragments/communicating.html)
     * for more information.
     */
    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onFragmentInteraction(uri: Uri)
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment StatsFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
                StatsFragment().apply {
                    arguments = Bundle().apply {
                        putString(ARG_PARAM1, param1)
                        putString(ARG_PARAM2, param2)
                    }
                }
    }
}
