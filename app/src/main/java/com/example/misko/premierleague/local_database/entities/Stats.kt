package com.example.misko.premierleague.local_database.entities

import androidx.room.Entity

@Entity(primaryKeys = ["id", "home", "lastSeason"]
)
data class Stats(
        val id: Long = 0,
        val home: Long = 0,
        val lastSeason: Long = 0,
        val draw: Int,
        val goalsAgainst: Int,
        val goalsFor: Int,
        val lose: Int,
        val matchsPlayed: Int,
        val win: Int
)