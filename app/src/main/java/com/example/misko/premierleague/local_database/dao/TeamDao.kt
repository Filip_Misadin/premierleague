package com.example.misko.premierleague.local_database.dao

import androidx.room.*
import com.example.misko.premierleague.local_database.entities.Team

@Dao
interface TeamDao{
    @Query("SELECT * FROM team")
    fun getAll(): List<Team>

    @Query("SELECT * FROM team ORDER BY points DESC")
    fun getAllSortedByPoints(): List<Team>

    @Query("SELECT * FROM team WHERE id LIKE :id")
    fun findById(id: Long?): Team?

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(vararg team: Team)

    @Delete
    fun delete(team: Team)

    @Update
    fun updateTeam(vararg team: Team)



}