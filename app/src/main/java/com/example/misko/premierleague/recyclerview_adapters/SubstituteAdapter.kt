package com.example.misko.premierleague.recyclerview_adapters

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import com.example.misko.premierleague.R
import com.example.misko.premierleague.api.data.Player
import kotlinx.android.synthetic.main.view_subbed_player.view.*

class SubstituteAdapter(private var viewlist: List<Player>): androidx.recyclerview.widget.RecyclerView.Adapter<CustomViewHolder>() {

    // numberOfItems
    override fun getItemCount(): Int {
        return viewlist.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val cellForRow = layoutInflater.inflate(R.layout.view_subbed_player, parent, false)
        return CustomViewHolder(cellForRow)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: CustomViewHolder, position: Int) {
        val player = viewlist[position]
        var name = player.player
        if(player.player.length > 15){
            name = name.substring(0,14) + "."
        }

        holder.view.txtPlayerNumber.text = player.number
        holder.view.txtPlayerIn.text = name
        holder.view.txtPlayerOut.text = ""
    }

}