package com.example.misko.premierleague.api.data

data class Score(
        val extratime: Any,
        val fulltime: Any,
        val halftime: String,
        val penalty: Any
)