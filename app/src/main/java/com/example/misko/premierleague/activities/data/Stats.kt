package com.example.misko.premierleague.activities.data

class Stats(
        var draw: Int,
        var goalsAgainst: Int,
        var goalsFor: Int,
        var lose: Int,
        var matchsPlayed: Int,
        var win: Int
)