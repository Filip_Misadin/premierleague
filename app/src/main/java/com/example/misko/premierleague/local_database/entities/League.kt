package com.example.misko.premierleague.local_database.entities

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class League(
    @PrimaryKey(autoGenerate = true) val id: Long,
    val name: String,
    val country: String,
    val seasonStart: String,
    val seasonEnd: String,
    val logo: String,
    val gamesPlayed: Int,
    val avgGoalsHome: Double,
    val avgGoalsAway: Double,
    val percentageWinsHome: Double,
    val percentageDraws: Double,
    val percentageWinsAway: Double)