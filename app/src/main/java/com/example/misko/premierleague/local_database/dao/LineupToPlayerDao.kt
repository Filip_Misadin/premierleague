package com.example.misko.premierleague.local_database.dao

import androidx.room.*
import com.example.misko.premierleague.local_database.entities.LineupToPlayer

@Dao
interface LineupToPlayerDao{
    @Query("SELECT * FROM lineupToPlayer")
    fun getAll(): List<LineupToPlayer>

    @Query("SELECT * FROM lineupToPlayer WHERE lineupId LIKE :lineupId")
    fun findById(lineupId: Long): LineupToPlayer

    @Query("SELECT * FROM lineupToPlayer WHERE lineupId LIKE :lineupId")
    fun findAllByLineupId(lineupId: Long): List<LineupToPlayer>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(vararg lineupToPlayer: LineupToPlayer)

    @Delete
    fun delete(lineupToPlayer: LineupToPlayer)

    @Update
    fun updateLineupToPlayer(vararg lineupToPlayer: LineupToPlayer)
}