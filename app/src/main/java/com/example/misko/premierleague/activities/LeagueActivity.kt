package com.example.misko.premierleague.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.room.Room
import com.example.misko.premierleague.fragments.*
import com.example.misko.premierleague.local_database.PremierLeagueDatabase

import com.example.misko.premierleague.local_database.entities.League as LeagueEntity
import com.example.misko.premierleague.local_database.entities.Match as MatchEntity
import com.example.misko.premierleague.local_database.entities.Team as TeamEntity
import com.example.misko.premierleague.local_database.entities.Stats as StatsEntity
import kotlinx.android.synthetic.main.activity_league.*
import kotlin.collections.HashMap
import android.view.View
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import com.example.misko.premierleague.*
import com.example.misko.premierleague.activities.data.League
import com.example.misko.premierleague.activities.data.MatchShort
import com.example.misko.premierleague.activities.data.TeamShort
import com.example.misko.premierleague.pager.adapters.LeaguePagerAdapter
import kotlinx.coroutines.*


class LeagueActivity : AppCompatActivity() {

    companion object {
        var standingsShort: MutableList<TeamShort>? = mutableListOf()
        var teamIDs = HashMap<Long, Int>()
        var leagueDetails: League? = null
        var resultsShort: MutableList<MatchShort>? = mutableListOf()
        var matchesShort: MutableList<MatchShort>? = mutableListOf()
    }

    private var pagerAdapter: LeaguePagerAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_league)

        setupPagerAdapter()
    }

    private fun setupPagerAdapter(){
        pagerAdapter = LeaguePagerAdapter(supportFragmentManager)
        pagerAdapter!!.addFragments(SplashFragment(), "Splash")
        pagerAdapter!!.addFragments(LeagueFragment(), "League")

        viewPagerMain.adapter = pagerAdapter
    }

    fun loadDataFromDatabase(){
        val database = buildDatabase()

        GlobalScope.launch(Dispatchers.IO) {
            loadAndStoreTeams(database)
            loadAndStoreMatches(database)
            loadAndStoreResults(database)
            loadAndStoreLeague(database)

            withContext(Dispatchers.Main){
                refreshFragments()
                showStatusBar()
            }
        }
    }

    private fun buildDatabase(): PremierLeagueDatabase{
        return Room.databaseBuilder(
            applicationContext,
            PremierLeagueDatabase::class.java, "premier-league.db"
        ).build()
    }

    private fun loadAndStoreTeams(database: PremierLeagueDatabase){
        val teams = loadTeamsSorted(database)
        for((position, team) in teams.withIndex()){
            val teamStats = loadTeamStats(database, team.id)
            standingsShort!!.add(createTeamShort(team, teamStats))

            teamIDs[team.id] = position - 1
        }
    }

    private fun loadTeamsSorted(database: PremierLeagueDatabase): List<TeamEntity>{
        return database.team().getAllSortedByPoints()
    }

    private fun loadTeamStats(database: PremierLeagueDatabase, teamId: Long?): List<StatsEntity>{
        return database.stats().findByIdFromThisSeason(teamId)
    }

    private fun createTeamShort(team: TeamEntity, teamStats: List<StatsEntity>): TeamShort{
        //stats are ordered by home column, making home stats first element of the list, and away stats second
        return TeamShort(
            team.id, team.name,
            teamStats[0].matchsPlayed + teamStats[1].matchsPlayed,
            teamStats[0].win + teamStats[1].win,
            teamStats[0].draw + teamStats[1].draw,
            teamStats[0].lose + teamStats[1].lose,
            teamStats[0].goalsFor + teamStats[1].goalsFor - teamStats[0].goalsAgainst - teamStats[1].goalsAgainst,
            team.points
        )
    }

    private fun loadAndStoreMatches(database: PremierLeagueDatabase){
        val matchesDB = database.match().getAllNotFinishedSorted()
        for(match in matchesDB){
            val homeTeam = database.team().findById(match.homeTeamId)
            val awayTeam = database.team().findById(match.awayTeamId)
            if(homeTeam != null && awayTeam != null)
                matchesShort!!.add(createMatchShort(match, homeTeam.name, awayTeam.name))
        }
    }

    private fun loadAndStoreResults(database: PremierLeagueDatabase){
        val resultsDB = database.match().getAllFinishedSorted()
        for(result in resultsDB){
            val homeTeam = database.team().findById(result.homeTeamId)
            val awayTeam = database.team().findById(result.awayTeamId)
            if(homeTeam != null && awayTeam != null)
                resultsShort!!.add(createMatchShort(result, homeTeam.name, awayTeam.name))
        }
    }

    private fun createMatchShort(match: MatchEntity, homeTeamName: String, awayTeamName: String): MatchShort{
        return MatchShort(
            match.id, match.date, homeTeamName, awayTeamName,
            match.goalsHome, match.goalsAway
        )
    }

    private fun loadAndStoreLeague(database: PremierLeagueDatabase){
        val league = database.league().findById(AppConstants.CURRENT_SEASON_ID)
        if(league != null)
            leagueDetails = createLeague(league)
    }

    private fun createLeague(league: LeagueEntity): League{
        return League(
            league.id,
            league.name,
            league.country,
            league.seasonStart,
            league.seasonEnd,
            league.logo,
            resultsShort!!.size,
            league.avgGoalsHome,
            league.avgGoalsAway,
            listOf(league.percentageWinsHome, league.percentageDraws, league.percentageWinsAway)
        )
    }

    private fun refreshFragments(){
        supportFragmentManager.inTransaction {
            val frg = supportFragmentManager.fragments[1]
            detach(frg)
            attach(frg)
        }
        viewPagerMain.setCurrentItem(1,false)
    }

    private inline fun FragmentManager.inTransaction(func: FragmentTransaction.() -> Unit) {
        val fragmentTransaction = beginTransaction()
        fragmentTransaction.func()
        fragmentTransaction.commit()
    }

    fun hideStatusBar(){
        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_FULLSCREEN
    }

    private fun showStatusBar(){
        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_VISIBLE
    }
}
