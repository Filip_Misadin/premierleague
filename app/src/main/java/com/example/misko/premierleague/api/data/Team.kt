package com.example.misko.premierleague.api.data

import com.google.gson.annotations.SerializedName

data class Team(
        val logo: String,
        @SerializedName("team_id")
        val teamId: Long,
        @SerializedName("team_name")
        val teamName: String
)