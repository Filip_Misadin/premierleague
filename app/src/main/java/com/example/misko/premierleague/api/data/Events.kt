package com.example.misko.premierleague.api.data

import com.google.gson.annotations.SerializedName

data class Events(
        val api: ApiE
)

data class ApiE(
        val events: List<Event>,
        val results: Int
)

data class Event(
        val detail: String,
        val elapsed: Int,
        val player: String,
        @SerializedName("team_id")
        val teamId: Any?,
        val teamName: String,
        val type: String
)