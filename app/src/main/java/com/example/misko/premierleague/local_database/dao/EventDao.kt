package com.example.misko.premierleague.local_database.dao

import androidx.room.*
import com.example.misko.premierleague.local_database.entities.Event

@Dao
interface EventDao{
    @Query("SELECT * FROM Event")
    fun getAll(): List<Event>

    @Query("SELECT * FROM event WHERE id LIKE :id")
    fun findById(id: Long): Event?

    @Query("SELECT * FROM event WHERE matchId LIKE :matchId")
    fun findByMatchId(matchId: Long): List<Event>

    @Insert
    fun insert(vararg event: Event)

    @Delete
    fun delete(event: Event)

    @Update
    fun updateEvent(vararg event: Event)
}