package com.example.misko.premierleague.api.data


import com.google.gson.annotations.SerializedName

data class Fixture(
        val awayTeam: Team,
        val elapsed: Int,
        @SerializedName("event_date")
        val eventDate: String,
        @SerializedName("event_timestamp")
        val eventTimestamp: Int,
        val firstHalfStart: Int,
        @SerializedName("fixture_id")
        val fixtureId: Long,
        val goalsAwayTeam: Int,
        val goalsHomeTeam: Int,
        val homeTeam: Team,
        @SerializedName("league_id")
        val leagueId: Int,
        val referee: Any,
        val round: String,
        val score: Score,
        val secondHalfStart: Int,
        val status: String,
        val statusShort: String,
        val venue: Any
)