package com.example.misko.premierleague.local_database.entities

import androidx.room.*

@Entity(
    indices = [Index("homeTeamId", unique=false),
                Index("awayTeamId", unique=false),
                Index("homeLineupId", unique=false),
                Index("awayLineupId", unique=false)],
            foreignKeys = [ForeignKey(
                entity = Team::class,
                parentColumns = arrayOf("id"),
                childColumns = arrayOf("homeTeamId")), ForeignKey(
                entity = Team::class,
                parentColumns = arrayOf("id"),
                childColumns = arrayOf("awayTeamId")), ForeignKey(
                entity = Lineup::class,
                parentColumns = arrayOf("id"),
                childColumns = arrayOf("homeLineupId")), ForeignKey(
                entity = Lineup::class,
                parentColumns = arrayOf("id"),
                childColumns = arrayOf("awayLineupId"))]
)
data class Match(@PrimaryKey val id: Long,
                 val date: String,
                 val homeTeamId: Long? = null,
                 val awayTeamId: Long? = null,
                 val status: String,
                 val goalsHome: Int,
                 val goalsAway: Int,
                 val halfTimeScore: String?,
                 val blockedShotsHome: Int,
                 val blockedShotsAway: Int,
                 val cornerKicksHome: Int,
                 val cornerKicksAway: Int,
                 val foulsHome: Int,
                 val foulsAway: Int,
                 val passesAccurateHome: Int,
                 val passesAccurateAway: Int,
                 val totalPassesHome: Int,
                 val totalPassesAway: Int,
                 val offsidesHome: Int,
                 val offsidesAway: Int,
                 val ballPossessionHome: String?,
                 val ballPossessionAway: String?,
                 val redCardsHome: String?,
                 val redCardsAway: String?,
                 val yellowCardsHome: String?,
                 val yellowCardsAway: String?,
                 val shotsOffGoalHome: Int,
                 val shotsOffGoalAway: Int,
                 val shotsOnGoalHome: Int,
                 val shotsOnGoalAway: Int,
                 val oddHome: Double,
                 val oddDraw: Double,
                 val oddAway: Double,
                 val homeLineupId: Long? = null,
                 val awayLineupId: Long? = null,
                 val headToHead: String?)