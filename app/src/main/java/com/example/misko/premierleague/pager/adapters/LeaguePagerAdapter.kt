package com.example.misko.premierleague.pager.adapters

import androidx.fragment.app.FragmentStatePagerAdapter

class LeaguePagerAdapter(fm: androidx.fragment.app.FragmentManager): FragmentStatePagerAdapter(fm,FragmentStatePagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {
    private var mFragmentItems:ArrayList<androidx.fragment.app.Fragment> = ArrayList()
    private var mFragmentTitles:ArrayList<String> = ArrayList()

    fun addFragments(fragmentItem: androidx.fragment.app.Fragment, fragmentTitle:String){
        mFragmentItems.add(fragmentItem)
        mFragmentTitles.add(fragmentTitle)
    }

    override fun getItem(position: Int): androidx.fragment.app.Fragment {
        return mFragmentItems[position]
    }

    override fun getCount(): Int {
        return mFragmentItems.size
    }

    override fun getPageTitle(position: Int): CharSequence {
        return mFragmentTitles[position]
    }

}