package com.example.misko.premierleague.activities.data

import com.example.misko.premierleague.api.data.Lineup
import java.io.Serializable

enum class MatchStatus{
    NS,FT,TBD
}

class Match(var id: Long,
            var date: String,
            var homeTeam: Team,
            var awayTeam: Team,
            var status: MatchStatus,
            var goalsHome: Int,
            var goalsAway: Int,
            var halfTimeScore: String?,
            var blockedShots: List<Int>,
            var cornerKicks: List<Int>,
            var fouls: List<Int>,
            var passesAccurate: List<Int>,
            var totalPasses: List<Int>,
            var offsides: List<Int>,
            var ballPossesion: List<String?>,
            var redCards: List<String?>,
            var yellowCards: List<String?>,
            var shotsOffGoal: List<Int>,
            var shotsOnGoal: List<Int>,
            var events: List<Event>,
            var odds: List<Double>,
            var homeLineup: Lineup,
            var awayLineup: Lineup,
            var headToHead: String?)

enum class EventType{
    CARD,GOAL,SUB
}

class MatchShort(var id: Long,
                 var date: String,
                 var homeTeam: String,
                 var awayTeam: String,
                 var goalsHome: Int,
                 var goalsAway: Int) : Serializable

class Event(var elapsed: Int,
            var teamName: String,
            var player: String,
            var type: EventType,
            var detail: String) : Serializable