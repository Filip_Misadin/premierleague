package com.example.misko.premierleague.local_database.entities

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Squad(
    @PrimaryKey val id: Long,
    val coach: String)
