package com.example.misko.premierleague.activities.data

class League(var id: Long,
             var name: String,
             var country: String,
             var seasonStart: String,
             var seasonEnd: String,
             var logo: String,
             var gamesPlayed: Int,
             var avgGoalsHome: Double,
             var avgGoalsAway: Double,
             var percentageWins: List<Double>)