package com.example.misko.premierleague.local_database.entities

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Lineup(
        @PrimaryKey(autoGenerate = true) val id: Long,
        val coach: String,
        val formation: String
)