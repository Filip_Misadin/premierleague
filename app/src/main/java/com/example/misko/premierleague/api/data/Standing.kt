package com.example.misko.premierleague.api.data

import com.google.gson.annotations.SerializedName

data class Standing(
        val all: TeamStats,
        val away: TeamStats,
        val description: String,
        val forme: String?,
        val goalsDiff: Int,
        val group: String,
        val home: TeamStats,
        val lastUpdate: String,
        val logo: String,
        val points: Int,
        val rank: Int,
        @SerializedName("team_id")
        val teamId: Long,
        val teamName: String
)