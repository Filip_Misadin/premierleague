package com.example.misko.premierleague.local_database.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey

@Entity(foreignKeys = [ForeignKey(
    entity = Match::class,
    parentColumns = arrayOf("id"),
    childColumns = arrayOf("matchId"))]
)
data class Event(
    @PrimaryKey(autoGenerate = true) val id: Long,
    @ColumnInfo(index = true) val matchId: Long?,
    val elapsed: Int,
    val teamName: String,
    val player: String,
    val type: String,
    val detail: String)