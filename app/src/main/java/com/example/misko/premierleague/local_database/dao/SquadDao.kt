package com.example.misko.premierleague.local_database.dao

import androidx.room.*
import com.example.misko.premierleague.local_database.entities.Squad

@Dao
interface SquadDao{
    @Query("SELECT * FROM squad")
    fun getAll(): List<Squad>

    @Query("SELECT * FROM squad WHERE id LIKE :id")
    fun findById(id: Long): Squad?

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(vararg squad: Squad)

    @Delete
    fun delete(squad: Squad)

    @Update
    fun updateSquad(vararg squad: Squad)
}