package com.example.misko.premierleague.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.room.Room
import com.example.misko.premierleague.*
import com.example.misko.premierleague.activities.data.*
import com.example.misko.premierleague.api.data.Player
import com.example.misko.premierleague.api.data.Squads
import com.example.misko.premierleague.fragments.*
import com.example.misko.premierleague.local_database.PremierLeagueDatabase
import com.example.misko.premierleague.local_database.entities.Coach
import com.example.misko.premierleague.pager.adapters.LeaguePagerAdapter
import com.example.misko.premierleague.local_database.entities.Player as PlayerEntity
import com.example.misko.premierleague.local_database.entities.Team as TeamEntity
import com.example.misko.premierleague.local_database.entities.Stats as StatsEntity
import com.example.misko.premierleague.local_database.entities.Match as MatchEntity
import com.example.misko.premierleague.api.services.ApiFactory
import com.example.misko.premierleague.api.services.SoccerApi
import kotlinx.android.synthetic.main.activity_team.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.yield
import retrofit2.Response

class TeamActivity : AppCompatActivity() {

    companion object {
        var team: Team?=null
    }

    private var pagerAdapter: LeaguePagerAdapter?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_team)

        val teamShort = intent.extras?.getSerializable("Team") as? TeamShort

        setupPagerAdapter()
        setupTabLayout()

        val database = buildDatabase()

        val service = ApiFactory.soccerApi
        GlobalScope.launch(Dispatchers.IO) {
            try {
                val (teamDB, stats) = loadTeamAndStats(database, teamShort?.id)
                team = createTeam(teamDB!!, stats)

                val squad = fetchSquad(service)
                val matchesDB = loadTeamMatches(database)

                team!!.squad = createSquad(squad!!)
                insertCoachesInDatabase(database, squad.api.coachs)
                insertPlayersInDatabase(database, squad.api.players)

                saveMatches(database, matchesDB)

                updateFragments()
            }catch (e: Exception){
                Log.d("TAG", "test$e")
            }
        }
    }

    private fun buildDatabase(): PremierLeagueDatabase{
        return Room.databaseBuilder(
            applicationContext,
            PremierLeagueDatabase::class.java, "premier-league.db"
        ).build()
    }

    private fun saveMatches(database: PremierLeagueDatabase, matches: List<MatchEntity>){
        team!!.matches = mutableListOf()
        for(match in matches){
            val homeTeam = loadTeam(database, match.homeTeamId)
            val awayTeam = loadTeam(database, match.awayTeamId)
            if(homeTeam != null && awayTeam != null)
                team!!.matches!!.add(createMatchShort(match, homeTeam.name, awayTeam.name))
        }
    }

    private fun insertCoachesInDatabase(database: PremierLeagueDatabase, coaches: List<String>){
        for(coach in coaches){
            database.coach().insert(Coach(0, coach, team!!.name))
        }
    }

    private fun insertPlayersInDatabase(database: PremierLeagueDatabase, players: List<Player>){
        for(player in players){
            database.player().insert(PlayerEntity(0, player.player, player.number, team!!.name))
        }
    }

    private suspend fun fetchSquad(service: SoccerApi): Squads?{
        val response = service.getSquadsAsync(team!!.id).await()
        if(checkResponse(response)){
            return response.body()
        }
        showErrorOnScreen("Fetching squad failed.")
        yield()
        return null
    }

    private fun <T> checkResponse(response: Response<T>): Boolean{
        if (!response.isSuccessful || response.body() == null)
            return false
        return true
    }

    private suspend fun loadTeamAndStats(database: PremierLeagueDatabase,
                                         teamId: Long?): Pair<TeamEntity?, List<StatsEntity>>{
        val team = database.team().findById(teamId)
        val stats = database.stats().findByIdFromThisSeason(team?.id)
        if(team == null || stats.isEmpty()){
            showErrorOnScreen("Loading team failed.")
            yield()
        }
        return Pair(team, stats)
    }

    private fun loadTeamMatches(database: PremierLeagueDatabase): List<MatchEntity>{
        return database.match().getAllByTeam(team!!.id)
    }

    private fun loadTeam(database: PremierLeagueDatabase, teamId: Long?): TeamEntity?{
        return database.team().findById(teamId)
    }

    private fun createTeam(teamDB: TeamEntity, stats: List<StatsEntity>): Team{
        return Team(
            teamDB.id, teamDB.name, teamDB.logo, teamDB.form,
            createStats(stats[0]), createStats(stats[1]),
            mutableListOf(), null, teamDB.points
        )
    }

    private fun createSquad(squad: Squads): Squad{
        return Squad(
            squad.api.coachs,
            squad.api.players
        )
    }

    private fun createStats(stats: StatsEntity): Stats{
        return Stats(
            stats.draw,
            stats.goalsAgainst,
            stats.goalsFor,
            stats.lose,
            stats.matchsPlayed,
            stats.win
        )
    }

    private fun createMatchShort(match: MatchEntity, homeTeamName: String, awayTeamName: String): MatchShort{
        return MatchShort(
            match.id,
            match.date,
            homeTeamName,
            awayTeamName,
            match.goalsHome,
            match.goalsAway
        )
    }

    private fun showErrorOnScreen(error: String){
        Toast.makeText(applicationContext, error, Toast.LENGTH_SHORT).show()
    }

    private fun setupPagerAdapter(){
        pagerAdapter = LeaguePagerAdapter(supportFragmentManager)
        pagerAdapter!!.addFragments(TeamDetailsFragment(), "Details")
        pagerAdapter!!.addFragments(TeamMatchesFragment(), "Matches")
        pagerAdapter!!.addFragments(SquadFragment(), "Squad")

        viewPager.adapter = pagerAdapter
    }

    private fun setupTabLayout(){
        tabLayout.setupWithViewPager(viewPager)
    }

    private fun updateFragments(){
        for(i in 1..supportFragmentManager.fragments.size){
            val ft = supportFragmentManager.beginTransaction()
            val frg = supportFragmentManager.fragments[i - 1]
            ft.detach(frg)
            ft.attach(frg)
            ft.commit()
        }
    }
}
