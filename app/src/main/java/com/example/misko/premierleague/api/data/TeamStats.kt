package com.example.misko.premierleague.api.data

data class TeamStats(
        val draw: Int,
        val goalsAgainst: Int,
        val goalsFor: Int,
        val lose: Int,
        val matchsPlayed: Int,
        val win: Int
)