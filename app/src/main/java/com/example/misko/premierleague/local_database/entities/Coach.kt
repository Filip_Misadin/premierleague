package com.example.misko.premierleague.local_database.entities

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Coach(
    @PrimaryKey(autoGenerate = true) val id: Long = 0,
    val name: String,
    val teamName: String
)