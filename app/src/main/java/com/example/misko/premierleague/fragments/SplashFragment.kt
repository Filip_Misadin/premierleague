package com.example.misko.premierleague.fragments

import android.content.Context
import android.content.SharedPreferences
import android.net.ConnectivityManager
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.room.Room
import com.example.misko.premierleague.*
import com.example.misko.premierleague.activities.LeagueActivity
import com.example.misko.premierleague.activities.data.MatchStatus
import com.example.misko.premierleague.api.data.*
import com.example.misko.premierleague.local_database.PremierLeagueDatabase
import com.example.misko.premierleague.api.services.ApiFactory
import com.example.misko.premierleague.api.services.SoccerApi
import kotlinx.coroutines.*
import retrofit2.Response
import kotlin.coroutines.CoroutineContext

import com.example.misko.premierleague.local_database.entities.League as LeagueEntity
import com.example.misko.premierleague.local_database.entities.Match as MatchEntity
import com.example.misko.premierleague.local_database.entities.Team as TeamEntity
import com.example.misko.premierleague.local_database.entities.Stats as StatsEntity


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [SplashFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [SplashFragment.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class SplashFragment : Fragment(), CoroutineScope {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    private var listener: OnFragmentInteractionListener? = null

    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Main + Job()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        (activity as LeagueActivity).hideStatusBar()
        return inflater.inflate(R.layout.fragment_splash, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val database = buildDatabase(view.context)

        val service = ApiFactory.soccerApi
        if(checkIfViewIsAlreadyCreated(savedInstanceState)){
            launch(Dispatchers.IO) {
                if(checkInternetConnection()){
                    try {
                        val (teams, numberOfTeams) = getTeams(service)

                        storeTeamsInDatabase(database, numberOfTeams, teams)

                        val fixturesList: List<Fixture> = getFixtures(service)

                        val sharedPref = activity!!.getPreferences(Context.MODE_PRIVATE)
                        val lastResultSavedId = sharedPref.getInt("lastResultNumber", 0)

                        val numberOfMatchesStored = storeFixturesInDatabase(database, service, fixturesList, lastResultSavedId)

                        saveLastResultStored(sharedPref, numberOfMatchesStored + lastResultSavedId)
                        val league = getLeague(service)

                        if(league != null)
                            storeLeagueInDatabase(database, league, numberOfMatchesStored + lastResultSavedId)
                    } catch (e: Exception) {
                        println("Error: $e")
                    }
                } else {
                    showErrorOnScreen("No internet access.")
                }

                withContext(Dispatchers.Main){
                    (activity as LeagueActivity).loadDataFromDatabase()
                }
            }
        }
    }

    private fun checkIfViewIsAlreadyCreated(savedInstanceState: Bundle?): Boolean{
        return savedInstanceState == null
    }

    private fun buildDatabase(context: Context): PremierLeagueDatabase{
        return Room.databaseBuilder(
            context,
            PremierLeagueDatabase::class.java, "premier-league.db"
        ).build()
    }

    private fun showErrorOnScreen(error: String){
        Toast.makeText(activity, error, Toast.LENGTH_SHORT).show()
    }

    private fun saveLastResultStored(sharedPref: SharedPreferences, lastResult: Int){
        val editor: SharedPreferences.Editor = sharedPref.edit()
        editor.putInt("lastResultNumber", lastResult)
        editor.apply()
    }

    private fun checkInternetConnection(): Boolean{
        val connectivityManager = activity!!.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkInfo = connectivityManager.activeNetworkInfo
        return  networkInfo!=null && networkInfo.isConnected
    }

    private fun insertTBDMatchInDatabase(database: PremierLeagueDatabase, fixture: Fixture,
                                         matchStatus: MatchStatus){
        database.match().insert(
            MatchEntity(
                fixture.fixtureId,
                fixture.eventDate,
                fixture.homeTeam.teamId,
                fixture.awayTeam.teamId,
                matchStatus.toString(),
                0,
                0,
                fixture.score.halftime,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                "0%",
                "0%",
                "0",
                "0",
                "0",
                "0",
                0,
                0,
                0,
                0,
                0.0,
                0.0,
                0.0,
                null,
                null,
                "0"
            )
        )
    }



    private fun insertMatchInDatabase(database: PremierLeagueDatabase, fixture: Fixture,
                                      matchStatus: MatchStatus, stats: Statistics
    ){
        val matchDB = database.match().findById(fixture.fixtureId)
        val oddsHome = matchDB?.oddHome ?: 0.0
        val oddsDraw = matchDB?.oddDraw ?: 0.0
        val oddsAway = matchDB?.oddAway ?: 0.0

        database.match().insert(
            MatchEntity(
                fixture.fixtureId,
                fixture.eventDate,
                fixture.homeTeam.teamId,
                fixture.awayTeam.teamId,
                matchStatus.toString(),
                fixture.goalsHomeTeam,
                fixture.goalsAwayTeam,
                fixture.score.halftime,
                stats.blockedShots.home?.toIntOrNull() ?: 0,
                stats.blockedShots.away?.toIntOrNull() ?: 0,
                stats.cornerKicks.home?.toIntOrNull() ?: 0,
                stats.cornerKicks.away?.toIntOrNull() ?: 0,
                stats.fouls.home?.toIntOrNull() ?: 0,
                stats.fouls.away?.toIntOrNull() ?: 0,
                stats.passesAccurate.home?.toIntOrNull() ?: 0,
                stats.passesAccurate.away?.toIntOrNull() ?: 0,
                stats.totalPasses.home?.toIntOrNull() ?: 0,
                stats.totalPasses.away?.toIntOrNull() ?: 0,
                stats.offsides.home?.toIntOrNull() ?: 0,
                stats.offsides.away?.toIntOrNull() ?: 0,
                stats.ballPossession.home ?: "0",
                stats.ballPossession.away ?: "0",
                stats.redCards.home ?: "0",
                stats.redCards.away ?: "0",
                stats.yellowCards.home ?: "0",
                stats.yellowCards.away ?: "0",
                stats.shotsOffGoal.home?.toIntOrNull() ?: 0,
                stats.shotsOffGoal.away?.toIntOrNull() ?: 0,
                stats.shotsOnGoal.home?.toIntOrNull() ?: 0,
                stats.shotsOnGoal.away?.toIntOrNull() ?: 0,
                oddsHome,
                oddsDraw,
                oddsAway,
                null,
                null,
                "0"
            )
        )
    }

    private suspend fun storeFixturesInDatabase(database: PremierLeagueDatabase, service: SoccerApi,
                                                fixturesList: List<Fixture>, lastResultSavedId: Int): Int{
        var numberOfMatches = 0
        for (j in lastResultSavedId..(lastResultSavedId + AppConstants.MAX_NUMBER_OF_MATCHES_TO_LOAD - 1)) {
            val matchStatus = when {
                fixturesList[j].statusShort == "FT" -> MatchStatus.FT
                else -> MatchStatus.TBD
            }

            if (matchStatus == MatchStatus.TBD) {
                if (database.match().findById(fixturesList[j].fixtureId) == null) {
                    insertTBDMatchInDatabase(database, fixturesList[j], matchStatus)
                }
            } else {
                val stats = getFixtureStats(service, fixturesList[j].fixtureId)
                if(stats != null)
                    insertMatchInDatabase(database, fixturesList[j], matchStatus, stats)
                numberOfMatches++
            }
        }
        return numberOfMatches
    }

    private fun storeLeagueInDatabase(database: PremierLeagueDatabase, league: LeagueDetails, matchesPlayed: Int){
        val statsHome = database.stats().getHomeStats()
        val statsAway = database.stats().getAwayStats()
        database.league().insert(LeagueEntity(league.leagueId, league.name,
            league.country,
            league.seasonStart, league.seasonEnd, league.logo,
            matchesPlayed,
            statsHome.goalsFor.toDouble() / statsHome.matchsPlayed,
            statsAway.goalsFor.toDouble() / statsAway.matchsPlayed,
            statsHome.win.toDouble() / matchesPlayed,
            statsHome.draw.toDouble() / matchesPlayed,
            statsAway.win.toDouble() / matchesPlayed))
        if(database.league().findById(AppConstants.LAST_SEASON_ID) == null)
            //last season
            database.league().insert(LeagueEntity(AppConstants.LAST_SEASON_ID, league.name,
                league.country,
                league.seasonStart, league.seasonEnd, league.logo,
                380, 1.58, 1.22,
                0.48, 0.18, 0.33))
    }

    private fun storeTeamsInDatabase(database: PremierLeagueDatabase, numberOfTeams: Int, teams: Standings){
        val standings = teams.api.standings[0]
        for (i in 1..numberOfTeams) {
            database.stats().insert(
                StatsEntity(
                    standings[i - 1].teamId, 1, 0,
                    standings[i - 1].home.draw,
                    standings[i - 1].home.goalsAgainst,
                    standings[i - 1].home.goalsFor,
                    standings[i - 1].home.lose,
                    standings[i - 1].home.matchsPlayed,
                    standings[i - 1].home.win
                )
            )
            database.stats().insert(
                StatsEntity(
                    standings[i - 1].teamId, 0, 0,
                    standings[i - 1].away.draw,
                    standings[i - 1].away.goalsAgainst,
                    standings[i - 1].away.goalsFor,
                    standings[i - 1].away.lose,
                    standings[i - 1].away.matchsPlayed,
                    standings[i - 1].away.win
                )
            )

            val teamDB = database.team().findById(standings[i - 1].teamId)
            val playersDB = database.player().getAllInTeam(standings[i - 1].teamName)
            val coachesDB = database.coach().getAllInTeam(standings[i - 1].teamName)
            val players: MutableList<Player> = mutableListOf()
            val coaches: MutableList<String> = mutableListOf()
            for (player in playersDB) {
                players.add(Player(player.number, player.name))
            }
            for (coach in coachesDB) {
                coaches.add(coach.name)
            }
            val form = teams.api.standings[0][i - 1].forme ?: ""
            var squad: Long? = null
            if(teamDB != null){
                squad = teamDB.squadId
            }
            database.team().insert(
                TeamEntity(
                    standings[i - 1].teamId,
                    standings[i - 1].teamName,
                    standings[i - 1].logo,
                    form,
                    squad,
                    standings[i - 1].points
                )
            )
        }
    }

    private fun <T> checkResponse(response: Response<T>): Boolean{
        if (!response.isSuccessful || response.body() == null)
            return false
        return true
    }

    private suspend fun getLeague(service: SoccerApi): LeagueDetails?{
        val response = service.getLeagueDetailsAsync(AppConstants.CURRENT_SEASON_ID).await()
        if(checkResponse(response)){
            return response.body()!!.api.leagues[0]
        }
        return null
    }

    private suspend fun getFixtureStats(service: SoccerApi, fixtureId: Long): Statistics?{
        val response = service.getFixtureStatsAsync(fixtureId).await()
        if(checkResponse(response)){
            return response.body()!!.api.statistics
        }
        return null
    }

    private suspend fun getFixtures(service: SoccerApi): List<Fixture>{
        val response = service.getLeagueFixturesAsync(AppConstants.CURRENT_SEASON_ID).await()
        if(checkResponse(response)){
            return response.body()!!.api.fixtures
        }
        return emptyList()
    }

    private suspend fun getTeams(service: SoccerApi): Pair<Standings, Int>{
        val response = service.getStandingsAsync().await()
        if (response.isSuccessful) {
            val teams = response.body()
            if (teams != null) {
                return Pair(teams, teams.api.standings[0].size)
            }
        }
        return Pair(Standings(), 0)
    }

    // TODO: Rename method, update argument and hook method into UI event
    fun onButtonPressed(uri: Uri) {
        listener?.onFragmentInteraction(uri)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments]
     * (http://developer.android.com/training/basics/fragments/communicating.html)
     * for more information.
     */
    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onFragmentInteraction(uri: Uri)
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment SplashFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            SplashFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}
