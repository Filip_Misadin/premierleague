package com.example.misko.premierleague.local_database

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.example.misko.premierleague.local_database.dao.*
import com.example.misko.premierleague.local_database.entities.*

@Database(entities = [League::class, Event::class, Lineup::class,
    LineupToPlayer::class, Match::class, Player::class, Squad::class,
    SquadToPlayer::class, Team::class, Stats::class, Coach::class], version = 1, exportSchema = false)
@TypeConverters(TypeConverter::class)
abstract class PremierLeagueDatabase : RoomDatabase() {
    abstract fun league(): LeagueDao
    abstract fun event(): EventDao
    abstract fun lineup(): LineupDao
    abstract fun lineupToPlayer(): LineupToPlayerDao
    abstract fun match(): MatchDao
    abstract fun player(): PlayerDao
    abstract fun squad(): SquadDao
    abstract fun squadToPlayer(): SquadToPlayerDao
    abstract fun team(): TeamDao
    abstract fun stats(): StatsDao
    abstract fun coach(): CoachDao
}