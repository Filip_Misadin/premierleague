package com.example.misko.premierleague.local_database.entities

import androidx.room.*

@Entity(indices = [Index("squadId", unique=true)],
    foreignKeys = [ForeignKey(
        entity = Squad::class,
        parentColumns = arrayOf("id"),
        childColumns = arrayOf("squadId"))]
)
data class Team(
    @PrimaryKey val id: Long,
    val name: String,
    val logo: String,
    val form: String,
    val squadId: Long? = null,
    val points: Int)



