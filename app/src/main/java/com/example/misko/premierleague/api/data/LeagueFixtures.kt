package com.example.misko.premierleague.api.data

data class LeagueFixtures(
        val api: ApiLF
)

data class ApiLF(
        val fixtures: List<Fixture>,
        val results: Int
)