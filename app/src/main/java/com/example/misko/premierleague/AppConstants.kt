package com.example.misko.premierleague

object AppConstants{
    const val API_BASE_URL = "https://api-football-v1.p.rapidapi.com/"
    const val API_KEY = BuildConfig.API_KEY
    const val CURRENT_SEASON_ID: Long = 524
    const val LAST_SEASON_ID: Long = 2
    const val MAX_NUMBER_OF_MATCHES_TO_LOAD = 10
}