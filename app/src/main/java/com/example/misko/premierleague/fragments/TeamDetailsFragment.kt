package com.example.misko.premierleague.fragments

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.example.misko.premierleague.R
import com.example.misko.premierleague.activities.data.Team
import com.example.misko.premierleague.activities.TeamActivity
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_team_details.view.*


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [DetailsFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [DetailsFragment.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class TeamDetailsFragment : androidx.fragment.app.Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    private var listener: OnFragmentInteractionListener? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    @SuppressLint("SetTextI18n")
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_team_details, container, false)

        val team = TeamActivity.team
        if(team != null){
            showForm(view, team.form)


            loadTeamIcon(team.logo, view.imgTeamIcon)


            showData(view, team)
        }

        return view
    }

    private fun showData(view: View, team: Team){
        view.txtTeamName.text = team.name
        view.txtMatchesPlayedHome.text = team.home.matchsPlayed.toString()
        view.txtMatchesPlayedAway.text = team.away.matchsPlayed.toString()
        view.txtWinsHome.text = team.home.win.toString()
        view.txtWinsAway.text = team.away.win.toString()
        view.txtDrawsHome.text = team.home.draw.toString()
        view.txtDrawsAway.text = team.away.draw.toString()
        view.txtLosesHome.text = team.home.lose.toString()
        view.txtLosesAway.text = team.away.lose.toString()
        view.txtGoalsScoredHome.text = team.home.goalsFor.toString()
        view.txtGoalsScoredAway.text = team.away.goalsFor.toString()
        view.txtGoalsConcededHome.text = team.home.goalsAgainst.toString()
        view.txtGoalsConcededAway.text = team.away.goalsAgainst.toString()
        view.txtGoalsAverageHome.text = String.format("%.2f", team.home.goalsFor.toDouble() / team.home.matchsPlayed)
        view.txtGoalsAverageAway.text = (team.away.goalsFor / team.away.matchsPlayed).toString()
    }

    private fun showForm(view: View, formString: String){
        val form = formString.split("")
        val formViews = arrayListOf(view.txtForm1, view.txtForm2, view.txtForm3, view.txtForm4, view.txtForm5)
        for(i in 1..(form.size - 2)){
            setForm(formViews[i - 1], i, form)
        }
        for(j in (form.size - 1)..5){
            formViews[j - 1].text = ""
            formViews[j - 1].setBackgroundColor(Color.WHITE)
        }
    }

    private fun loadTeamIcon(logo: String, view: ImageView){
        Picasso.get().load(logo).fit().into(view)
    }

    private fun setForm(view: TextView, i: Int, form: List<String>){
        view.text = form[i]
        when {
            form[i] == "W" -> view.setBackgroundColor(Color.GREEN)
            form[i] == "D" -> view.setBackgroundColor(Color.LTGRAY)
            else -> view.setBackgroundColor(Color.RED)
        }
    }

    // TODO: Rename method, update argument and hook method into UI event
    fun onButtonPressed(uri: Uri) {
        listener?.onFragmentInteraction(uri)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments]
     * (http://developer.android.com/training/basics/fragments/communicating.html)
     * for more information.
     */
    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onFragmentInteraction(uri: Uri)
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment DetailsFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
                DetailsFragment().apply {
                    arguments = Bundle().apply {
                        putString(ARG_PARAM1, param1)
                        putString(ARG_PARAM2, param2)
                    }
                }
    }
}
