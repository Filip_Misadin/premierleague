package com.example.misko.premierleague.local_database.entities

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Player(
    @PrimaryKey(autoGenerate = true) val id: Long,
    val number: String,
    val name: String,
    val teamName: String
)