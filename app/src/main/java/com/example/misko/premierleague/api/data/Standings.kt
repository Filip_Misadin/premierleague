package com.example.misko.premierleague.api.data

data class Standings(
        val api: ApiST = ApiST(
                -1,
                emptyList()
        )
)

data class ApiST(
        val results: Int,
        val standings: List<List<Standing>>
)