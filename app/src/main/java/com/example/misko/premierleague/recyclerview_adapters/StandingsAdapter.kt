package com.example.misko.premierleague.recyclerview_adapters

import android.annotation.SuppressLint
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.misko.premierleague.*
import com.example.misko.premierleague.activities.TeamActivity
import com.example.misko.premierleague.activities.data.TeamShort

import kotlinx.android.synthetic.main.view_standings.view.*

class StandingsAdapter(private var viewlist: MutableList<TeamShort>): androidx.recyclerview.widget.RecyclerView.Adapter<CustomViewHolder>() {

    // numberOfItems
    override fun getItemCount(): Int {
        return viewlist.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val cellForRow = layoutInflater.inflate(R.layout.view_standings, parent, false)
        return CustomViewHolder(cellForRow)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: CustomViewHolder, position: Int) {
        val team = viewlist[position]
        holder.view.txtRank.text = (position + 1).toString()
        var teamName = team.name
        if(teamName.contains("Manchester"))
            teamName = teamName.replace("Manchester", "Man.")
        if(teamName.contains("Crystal"))
            teamName = teamName.replace("Palace", "Pal.")
        holder.view.txtTeamName.text = teamName
        holder.view.txtGames.text = (team.matchesPlayed).toString()
        holder.view.txtWins.text = (team.gamesWon).toString()
        holder.view.txtDraws.text = (team.gamesDraw).toString()
        holder.view.txtLoses.text = (team.gamesLost).toString()
        holder.view.txtGoals.text = team.goalDifference.toString()
        holder.view.txtPoints.text = team.points.toString()

        holder.view.constraintLayoutStandings.setOnClickListener{
            val intent = Intent(it.context, TeamActivity::class.java)
            intent.putExtra("Team", team)
            it.context.startActivity(intent)
        }
    }
}

class CustomViewHolder(val view: View): androidx.recyclerview.widget.RecyclerView.ViewHolder(view)
