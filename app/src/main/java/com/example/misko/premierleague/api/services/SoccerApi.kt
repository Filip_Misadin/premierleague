package com.example.misko.premierleague.api.services

import com.example.misko.premierleague.api.data.*
import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path

interface SoccerApi {
    @GET("v2/leagueTable/524")
    fun getStandingsAsync(): Deferred<Response<Standings>>

    @GET("players/2018/{team_id}")
    fun getSquadsAsync(@Path("team_id") teamId: Long?): Deferred<Response<Squads>>

    @GET("v2/leagues/league/{league_id}")
    fun getLeagueDetailsAsync(@Path("league_id") leagueId: Long?): Deferred<Response<League>>

    @GET("v2/fixtures/league/{league_id}")
    fun getLeagueFixturesAsync(@Path("league_id") leagueId: Long?): Deferred<Response<LeagueFixtures>>

    @GET("v2/statistics/fixture/{fixture_id}")
    fun getFixtureStatsAsync(@Path("fixture_id") fixtureId: Long?): Deferred<Response<FixtureStats>>

    @GET("v2/events/{fixture_id}")
    fun getFixtureEventsAsync(@Path("fixture_id") fixtureId: Long?): Deferred<Response<Events>>

    @GET("v2/lineups/{fixture_id}")
    fun getFixtureLineupsAsync(@Path("fixture_id") fixtureId: Long?): Deferred<Response<Lineups>>

    @GET("v2/fixtures/h2h/{team_1}/{team_2}")
    fun getHeadToHeadAsync(@Path("team_1") team1: Long?, @Path("team_2") team2: Long?)
                : Deferred<Response<HeadToHead>>

    @GET("v2/statistics/{league_id}/{team_id}")
    fun getStatsFromLastSeasonAsync(@Path("team_id") teamId: Long?, @Path("league_id") leagueId: Long?)
            : Deferred<Response<TeamStatsBySeason>>
}

