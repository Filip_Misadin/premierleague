package com.example.misko.premierleague.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.room.Room
import com.example.misko.premierleague.*
import com.example.misko.premierleague.activities.data.*
import com.example.misko.premierleague.activities.data.Event
import com.example.misko.premierleague.activities.data.League
import com.example.misko.premierleague.activities.data.Team
import com.example.misko.premierleague.api.data.*
import com.example.misko.premierleague.api.data.Event as EventApi
import com.example.misko.premierleague.fragments.*
import com.example.misko.premierleague.local_database.PremierLeagueDatabase
import com.example.misko.premierleague.pager.adapters.LeaguePagerAdapter
import com.example.misko.premierleague.api.services.ApiFactory
import com.example.misko.premierleague.api.services.SoccerApi
import kotlinx.android.synthetic.main.activity_match.*
import com.example.misko.premierleague.local_database.entities.Match as MatchEntity
import com.example.misko.premierleague.local_database.entities.Event as EventEntity
import com.example.misko.premierleague.local_database.entities.Team as TeamEntity
import com.example.misko.premierleague.local_database.entities.Stats as StatsEntity
import com.example.misko.premierleague.local_database.entities.Player as PlayerEntity
import com.example.misko.premierleague.local_database.entities.Lineup as LineupEntity
import com.example.misko.premierleague.local_database.entities.League as LeagueEntity
import com.example.misko.premierleague.local_database.entities.LineupToPlayer as LineupToPlayerEntity
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.yield
import retrofit2.Response
import kotlin.math.E
import kotlin.math.pow

class MatchActivity : AppCompatActivity() {

    companion object {
        var match: Match?=null
    }

    private var pagerAdapter: LeaguePagerAdapter?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_match)

        val matchShort = intent.extras?.getSerializable("Match") as? MatchShort

        setupViewPager()
        setupTabLayout()

        val database = buildDatabase()

        val service = ApiFactory.soccerApi
        GlobalScope.launch(Dispatchers.IO) {
            try {
                val matchDB = loadMatchFromDatabase(database, matchShort!!.id)

                val (homeTeam, awayTeam) = loadTeamsFromDatabase(database, matchDB!!.homeTeamId, matchDB.awayTeamId)
                val (homeStats, awayStats) = loadTeamStatsFromDatabase(database, matchDB.homeTeamId, matchDB.awayTeamId)

                val matchStatus = getMatchStatus(matchDB)

                match = constructMatch(matchDB, homeTeam!!, awayTeam!!, homeStats, awayStats, matchStatus)

                processAndSaveMatchData(database, service, matchDB, homeTeam.id, awayTeam.id)

                updateFragments()
            }catch (e: Exception){
                Log.d("TAG", "test$e")
            }
        }
    }

    private fun buildDatabase(): PremierLeagueDatabase{
        return Room.databaseBuilder(
            applicationContext,
            PremierLeagueDatabase::class.java, "premier-league.db"
        ).build()
    }

    private suspend fun processAndSaveMatchData(database: PremierLeagueDatabase, service: SoccerApi,
                                                matchDB: MatchEntity, homeTeamId: Long?, awayTeamId: Long?){
        if(checkIfMatchStatsAreSaved(matchDB)) {
            val h2h = fetchHeadToHead(service, homeTeamId, awayTeamId)
            saveHeadToHeadFromApi(h2h)

            if (checkIfMatchIsOver(matchDB)) {
                handleFTMatch(database, service, matchDB.id)
            } else {
                handleTBDMatch(database, service, homeTeamId, awayTeamId)
            }
        } else {
            match!!.headToHead = matchDB.headToHead
            val events = loadEventsFromDatabase(database, matchDB.id)
            saveEventsFromDatabase(events)

            if(checkIfLineupIdsAreValid(matchDB.homeLineupId, matchDB.awayLineupId)){
                loadLineupsFromDatabase(database, matchDB)
            }

            match!!.odds = listOf(matchDB.oddHome, matchDB.oddDraw, matchDB.oddAway)
        }
    }

    private fun checkIfLineupIdsAreValid(homeLineupId: Long?, awayLineupId: Long?): Boolean{
        return homeLineupId != null && awayLineupId != null
    }

    private fun checkIfMatchStatsAreSaved(match: MatchEntity): Boolean{
        return match.headToHead == "0"
    }

    private fun checkIfMatchIsOver(match: MatchEntity): Boolean{
        return match.status == "FT"
    }

    private fun checkIfLastSeasonTeamStatsExist(database: PremierLeagueDatabase, teamId: Long?): Boolean{
        return database.stats().findByIdFromLastSeason(teamId).isEmpty()
    }

    private fun checkIfLineupContainsTeam(lineups: Map<String, Lineup>, teamName: String): Boolean{
        return lineups.containsKey(teamName)
    }

    private suspend fun handleFTMatch(database: PremierLeagueDatabase, service: SoccerApi,
                                      matchId: Long?){
        val events = fetchEvents(service, matchId)
        val lineups = fetchLineups(service, matchId)

        saveEventsFromApi(database, events)

        val (homeLineupId, awayLineupId) = saveLineupsFromApi(database, lineups)

        updateMatchInDatabase(database, homeLineupId, awayLineupId)
    }

    private suspend fun handleTBDMatch(database: PremierLeagueDatabase, service: SoccerApi,
                                       homeTeamId: Long?, awayTeamId: Long?){
        getLastSeasonTeamStats(service, database, homeTeamId)
        getLastSeasonTeamStats(service, database, awayTeamId)

        val league = loadLeagueFromDatabase(database, AppConstants.LAST_SEASON_ID)
        league?.let {
            calculateOdds(createLeague(league))
        }

        updateMatchInDatabase(database, null, null)
    }

    private suspend fun getLastSeasonTeamStats(service: SoccerApi, database: PremierLeagueDatabase,
                                               teamId: Long?){
        if(checkIfLastSeasonTeamStatsExist(database, teamId)){
            val stats = fetchTeamStats(service, teamId, AppConstants.LAST_SEASON_ID)!!

            saveStatsFromApi(stats, match!!.homeTeam)
        } else {
            val stats = loadLastSeasonTeamStatsFromDatabase(database, teamId)

            saveStatsFromDatabase(stats, match!!.homeTeam)
        }
    }

    private fun <T> checkResponse(response: Response<T>): Boolean{
        if (!response.isSuccessful || response.body() == null)
            return false
        return true
    }

    private suspend fun fetchLineups(service: SoccerApi, matchId: Long?): Map<String, Lineup>{
        val response = service.getFixtureLineupsAsync(matchId).await()
        if(checkResponse(response))
            return response.body()!!.api.lineUps
        showErrorOnScreen("Fetching lineups failed.")
        yield()
        return mapOf()
    }

    private suspend fun fetchHeadToHead(service: SoccerApi, homeTeamId: Long?, awayTeamId: Long?): List<Fixture>{
        val response = service.getHeadToHeadAsync(homeTeamId, awayTeamId).await()
        if(checkResponse(response))
            return response.body()!!.api.fixtures
        showErrorOnScreen("Fetching h2h failed.")
        yield()
        return emptyList()
    }

    private suspend fun fetchEvents(service: SoccerApi, matchId: Long?): List<EventApi>{
        val response = service.getFixtureEventsAsync(matchId).await()
        if(checkResponse(response))
            return response.body()!!.api.events
        showErrorOnScreen("Fetching events failed.")
        yield()
        return emptyList()
    }

    private suspend fun fetchTeamStats(service: SoccerApi, teamId: Long?, leagueId: Long): TeamStatistics? {
        val response = service.getStatsFromLastSeasonAsync(teamId, leagueId).await()
        if(checkResponse(response))
            return response.body()!!.api.statistics
        showErrorOnScreen("Fetching stats failed.")
        yield()
        return null
    }

    private fun updateFragments(){
        for(i in 1..supportFragmentManager.fragments.size){
            val ft = supportFragmentManager.beginTransaction()
            val frg = supportFragmentManager.fragments[i - 1]
            ft.detach(frg)
            ft.attach(frg)
            ft.commit()
        }
    }

    private fun updateMatchInDatabase(database: PremierLeagueDatabase, homeLineupId: Long?, awayLineupId: Long?){
        database.match().updateMatch(createMatchEntity(homeLineupId, awayLineupId))
    }

    private fun loadLeagueFromDatabase(database: PremierLeagueDatabase, id: Long): LeagueEntity?{
        return database.league().findById(id)
    }

    private fun loadEventsFromDatabase(database: PremierLeagueDatabase, id: Long): List<EventEntity>{
        return database.event().findByMatchId(id)
    }

    private fun loadLineupFromDatabase(database: PremierLeagueDatabase, lineupId: Long?): LineupEntity{
        return database.lineup().findById(lineupId)
    }

    private fun loadLineupToPlayerFromDatabase(database: PremierLeagueDatabase,
                                               lineupId: Long): List<LineupToPlayerEntity>{
        return database.lineupToPlayer().findAllByLineupId(lineupId)
    }

    private fun loadPlayerFromDatabase(database: PremierLeagueDatabase,
                                       playerId: Long): PlayerEntity?{
        return database.player().findById(playerId)
    }

    private fun loadTeamFromDatabase(database: PremierLeagueDatabase,
                                     teamId: Long?): TeamEntity?{
        return database.team().findById(teamId)
    }

    private fun loadLastSeasonTeamStatsFromDatabase(database: PremierLeagueDatabase, teamId: Long?): List<StatsEntity>{
        return database.stats().findByIdFromLastSeason(teamId)
    }

    private fun loadThisSeasonTeamStatsFromDatabase(database: PremierLeagueDatabase, teamId: Long?): List<StatsEntity>{
        return database.stats().findByIdFromThisSeason(teamId)
    }

    private fun saveEventsFromApi(database: PremierLeagueDatabase, events: List<EventApi>){
        val eventsList = mutableListOf<Event>()
        for (event in events) {
            val eventType = getEventTypeApi(event)
            eventsList.add(createEventApi(event, eventType))

            insertEventInDatabase(database, event)
        }
        match!!.events = eventsList
    }

    private fun saveHeadToHeadFromApi(h2h: List<Fixture>){
        var h2hValue = "0"
        for (i in 1..h2h.size) {
            if (h2h[i - 1].statusShort != "FT") {
                h2hValue = getHeadToHead(h2h[i - 2].goalsHomeTeam, h2h[i - 2].goalsAwayTeam)
                break
            }
        }
        match!!.headToHead = h2hValue
    }

    private fun saveStatsFromApi(stats: TeamStatistics, team: Team){
        team.home.draw += stats.matchs.draws.home
        team.home.goalsAgainst += stats.goals.goalsAgainst.home
        team.home.goalsFor += stats.goals.goalsFor.home
        team.home.lose += stats.matchs.loses.home
        team.home.matchsPlayed += stats.matchs.matchsPlayed.home
        team.home.win += stats.matchs.wins.home

        team.away.draw += stats.matchs.draws.away
        team.away.goalsAgainst += stats.goals.goalsAgainst.away
        team.away.goalsFor += stats.goals.goalsFor.away
        team.away.lose += stats.matchs.loses.away
        team.away.matchsPlayed += stats.matchs.matchsPlayed.away
        team.away.win += stats.matchs.wins.away
    }

    private fun saveStatsFromDatabase(stats: List<StatsEntity>, team: Team){
        //stats are ordered by home column, making home stats first element of the list, and away stats second
        if(stats.isEmpty()){
            showErrorOnScreen("Saving stats failed.")
            return
        }

        team.home.draw += stats[0].draw
        team.home.goalsAgainst += stats[0].goalsAgainst
        team.home.goalsFor += stats[0].goalsFor
        team.home.lose += stats[0].lose
        team.home.matchsPlayed += stats[0].matchsPlayed
        team.home.win += stats[0].win

        team.away.draw += stats[1].draw
        team.away.goalsAgainst += stats[1].goalsAgainst
        team.away.goalsFor += stats[1].goalsFor
        team.away.lose += stats[1].lose
        team.away.matchsPlayed += stats[1].matchsPlayed
        team.away.win += stats[1].win
    }

    private fun saveLineupsFromApi(database: PremierLeagueDatabase, lineups: Map<String, Lineup>): Pair<Long, Long>{
        var homeLineupId: Long = 0
        var awayLineupId: Long = 0
        if (checkIfLineupContainsTeam(lineups, match!!.homeTeam.name)) {
            match!!.homeLineup = lineups.getValue(match!!.homeTeam.name)
            match!!.awayLineup = lineups.getValue(match!!.awayTeam.name)

            homeLineupId = insertLineupInDatabase(database, match!!.homeLineup.coach, match!!.homeLineup.formation)
            insertPlayersInDatabase(database, match!!.homeLineup.startXI, homeLineupId)

            awayLineupId = insertLineupInDatabase(database, match!!.awayLineup.coach, match!!.awayLineup.formation)
            insertPlayersInDatabase(database, match!!.awayLineup.startXI, awayLineupId)
        }
        return Pair(homeLineupId, awayLineupId)
    }

    private fun saveEventsFromDatabase(events: List<EventEntity>){
        val eventsList = mutableListOf<Event>()
        for (event in events) {
            val eventType = getEventType(event)

            eventsList.add(createEventDatabase(event, eventType))
        }
        match!!.events = eventsList
    }

    private fun createMatchEntity(homeLineupId: Long?, awayLineupId: Long?): MatchEntity{
        return MatchEntity(
            match!!.id, match!!.date,
            match!!.homeTeam.id, match!!.awayTeam.id, "FT", match!!.goalsHome,
            match!!.goalsAway, match!!.halfTimeScore, match!!.blockedShots[0],
            match!!.blockedShots[1], match!!.cornerKicks[0], match!!.cornerKicks[1],
            match!!.fouls[0], match!!.fouls[1], match!!.passesAccurate[0],
            match!!.passesAccurate[1], match!!.totalPasses[0], match!!.totalPasses[1],
            match!!.offsides[0], match!!.offsides[1], match!!.ballPossesion[0],
            match!!.ballPossesion[1], match!!.redCards[0], match!!.redCards[1],
            match!!.yellowCards[0], match!!.yellowCards[1], match!!.shotsOffGoal[0],
            match!!.shotsOffGoal[1], match!!.shotsOnGoal[0], match!!.shotsOnGoal[1],
            match!!.odds[0], match!!.odds[1], match!!.odds[2], homeLineupId, awayLineupId,
            match!!.headToHead)
    }

    private fun createLeague(league: LeagueEntity): League{
        return League(
            league.id, league.name, league.country, league.seasonStart, league.seasonEnd,
            league.logo, league.gamesPlayed, league.avgGoalsHome, league.avgGoalsAway,
            listOf(
                league.percentageWinsHome,
                league.percentageDraws,
                league.percentageWinsAway
            )
        )
    }

    private fun createEventEntity(event: EventApi): EventEntity{
        return EventEntity(
            0, match!!.id,
            event.elapsed,
            event.teamName,
            event.player,
            event.type,
            event.detail
        )
    }

    private fun createEventDatabase(event: EventEntity, eventType: EventType): Event{
        return Event(
            event.elapsed,
            event.teamName,
            event.player,
            eventType,
            event.detail
        )
    }

    private fun createEventApi(event: EventApi, eventType: EventType): Event{
        return Event(
            event.elapsed,
            event.teamName,
            event.player,
            eventType,
            event.detail
        )
    }

    private fun createStats(stats: StatsEntity): Stats{
        return Stats(
            stats.draw,
            stats.goalsAgainst,
            stats.goalsFor,
            stats.lose,
            stats.matchsPlayed,
            stats.win
        )
    }

    private fun createTeam(team: TeamEntity, homeStats: StatsEntity, awayStats: StatsEntity): Team{
        return Team(
            team.id, team.name, team.logo, team.form,
            createStats(homeStats),
            createStats(awayStats),
            mutableListOf(), null, team.points
        )
    }
    private fun createLineupEntity(lineupId: Long, coach: String, formation: String): LineupEntity{
        return LineupEntity(
            lineupId,
            coach,
            formation
        )
    }

    private fun getEventType(event: EventEntity): EventType{
        return when {
            event.type == "Card" -> EventType.CARD
            event.type == "subst" -> EventType.SUB
            else -> EventType.GOAL
        }
    }

    private fun getEventTypeApi(event: EventApi): EventType{
        return when {
            event.type == "Card" -> EventType.CARD
            event.type == "subst" -> EventType.SUB
            else -> EventType.GOAL
        }
    }

    private fun getMatchStatus(match: MatchEntity): MatchStatus{
        return when {
            match.status == "FT" -> MatchStatus.FT
            else -> MatchStatus.TBD
        }
    }

    private fun getHeadToHead(goalsHomeTeam: Int, goalsAwayTeam: Int): String{
        return when {
            goalsHomeTeam > goalsAwayTeam -> "1"
            goalsHomeTeam == goalsAwayTeam -> "X"
            else -> "2"
        }
    }

    private fun insertEventInDatabase(database: PremierLeagueDatabase, event: EventApi){
        database.event().insert(createEventEntity(event))
    }

    private fun insertPlayersInDatabase(database: PremierLeagueDatabase, startXI: MutableList<Player>,
                                        lineupId: Long){
        for(player in startXI){
            val playerId = database.player().insert(PlayerEntity(0, player.number,
                player.player, match!!.awayTeam.name))
            database.lineupToPlayer().insert(LineupToPlayerEntity(lineupId, playerId, 1))
        }
    }

    private fun insertLineupInDatabase(database: PremierLeagueDatabase, coach: String, formation: String): Long{
        return database.lineup().insert(createLineupEntity(0,coach, formation))
    }

    private fun createLineup(lineup: LineupEntity, players: MutableList<Player>): Lineup{
        return Lineup(
            lineup.coach,
            lineup.formation,
            players,
            mutableListOf()
        )
    }

    private fun loadPlayersFromDatabase(database: PremierLeagueDatabase,
                            lineupToPlayer: List<LineupToPlayerEntity>): MutableList<Player>{
        val players = mutableListOf<Player>()
        for(lineup in lineupToPlayer){
            val player = loadPlayerFromDatabase(database, lineup.playerId)
            player?.let {
                players.add(Player(player.number, player.name))
            }
        }
        return players
    }

    private fun constructMatch(match: MatchEntity, homeTeam: TeamEntity, awayTeam: TeamEntity,
                               homeStats: List<StatsEntity>, awayStats: List<StatsEntity>,
                               matchStatus: MatchStatus): Match{
        return Match(
            match.id, match.date,
            createTeam(homeTeam, homeStats[0], homeStats[1]),
            createTeam(awayTeam, awayStats[0], awayStats[1]),
            matchStatus,
            match.goalsHome,
            match.goalsAway,
            match.halfTimeScore,
            listOf(match.blockedShotsHome, match.blockedShotsAway),
            listOf(match.cornerKicksHome, match.cornerKicksAway),
            listOf(match.foulsHome, match.foulsAway),
            listOf(
                match.passesAccurateHome,
                match.passesAccurateAway
            ),
            listOf(match.totalPassesHome, match.totalPassesAway),
            listOf(match.offsidesHome, match.offsidesAway),
            listOf(match.ballPossessionHome, match.ballPossessionAway),
            listOf(match.redCardsHome, match.redCardsAway),
            listOf(match.yellowCardsHome, match.yellowCardsAway),
            listOf(match.shotsOffGoalHome, match.shotsOffGoalAway),
            listOf(match.shotsOnGoalHome, match.shotsOnGoalHome),
            emptyList(),
            listOf(match.oddHome, match.oddDraw, match.oddAway),
            Lineup("", "", mutableListOf(), mutableListOf()),
            Lineup("", "", mutableListOf(), mutableListOf()),
            "0"
        )
    }

    private fun loadLineupsFromDatabase(database: PremierLeagueDatabase, matchDB: MatchEntity){
        val homeLineupDB = loadLineupFromDatabase(database, matchDB.homeLineupId)
        val awayLineupDB = loadLineupFromDatabase(database, matchDB.awayLineupId)

        val homeLineupToPlayerDB = loadLineupToPlayerFromDatabase(database, homeLineupDB.id)
        val awayLineupToPlayerDB = loadLineupToPlayerFromDatabase(database, awayLineupDB.id)

        val homePlayers = loadPlayersFromDatabase(database, homeLineupToPlayerDB)
        val awayPlayers = loadPlayersFromDatabase(database, awayLineupToPlayerDB)

        match!!.homeLineup = createLineup(homeLineupDB, homePlayers)
        match!!.awayLineup = createLineup(awayLineupDB, awayPlayers)
    }

    private suspend fun loadTeamsFromDatabase(database: PremierLeagueDatabase, homeTeamId: Long?,
                                      awayTeamId: Long?): Pair<TeamEntity?, TeamEntity?>{
        val homeTeam = loadTeamFromDatabase(database, homeTeamId)
        val awayTeam = loadTeamFromDatabase(database, awayTeamId)
        if(homeTeam == null || awayTeam == null){
            showErrorOnScreen("Loading teams failed.")
            yield()
        }
        return Pair(homeTeam, awayTeam)
    }

    private suspend fun loadTeamStatsFromDatabase(database: PremierLeagueDatabase, homeTeamId: Long?,
                                      awayTeamId: Long?): Pair<List<StatsEntity>, List<StatsEntity>>{
        val homeStats = loadThisSeasonTeamStatsFromDatabase(database, homeTeamId)
        val awayStats = loadThisSeasonTeamStatsFromDatabase(database, awayTeamId)
        if(homeStats.isNotEmpty() && awayStats.isNotEmpty())
            return Pair(homeStats, awayStats)
        showErrorOnScreen("Loading teams failed.")
        yield()
        return Pair(emptyList(), emptyList())
    }

    private suspend fun loadMatchFromDatabase(database: PremierLeagueDatabase, matchId: Long): MatchEntity?{
        val match = database.match().findById(matchId)
        if(match == null){
            showErrorOnScreen("Match not found.")
            //exit coroutine
            yield()
        }
        return database.match().findById(matchId)
    }

    private fun showErrorOnScreen(error: String){
        Toast.makeText(applicationContext, error, Toast.LENGTH_SHORT).show()
    }

    private fun setupViewPager(){
        pagerAdapter = LeaguePagerAdapter(supportFragmentManager)
        pagerAdapter!!.addFragments(DetailsFragment(), "Details")
        pagerAdapter!!.addFragments(StatsFragment(), "Stats")
        pagerAdapter!!.addFragments(LineupsFragment(), "Lineups")

        viewPager.adapter = pagerAdapter
    }

    private fun setupTabLayout(){
        tabLayout.setupWithViewPager(viewPager)
    }

    private fun calculateAverageGoals(goalsFor: Int, matchesPlayed: Int, leagueAvgGoals: Double): Double{
        val average = goalsFor.toDouble() / matchesPlayed / leagueAvgGoals
        return if(average == 0.0) 1.0 else average
    }

    private fun calculateOdds(league: League){
        val avgGoalsHome = calculateAverageGoals(match!!.homeTeam.home.goalsFor,
                                                 match!!.homeTeam.home.matchsPlayed, league.avgGoalsHome)
        val avgGoalsAway = calculateAverageGoals(match!!.awayTeam.away.goalsFor,
                                                 match!!.awayTeam.away.matchsPlayed, league.avgGoalsAway)
        val avgDefendHome = calculateAverageGoals(match!!.homeTeam.home.goalsFor,
                                                  match!!.homeTeam.home.matchsPlayed, league.avgGoalsAway)
        val avgDefendAway = calculateAverageGoals(match!!.awayTeam.away.goalsFor,
                                                  match!!.awayTeam.away.matchsPlayed, league.avgGoalsHome)

        val poissonList = calculatePoissonOdds(avgGoalsHome, avgGoalsAway, avgDefendHome, avgDefendAway, league)

        val (homeOdds, drawOdds, awayOdds) = sumPoissonOdds(poissonList)

        val homeMultiplier = getMultiplier(match!!.homeTeam, 1)
        val awayMultiplier = getMultiplier(match!!.awayTeam, 2)
        var drawMultiplier = 1.0
        if(match!!.headToHead == "X")
            drawMultiplier += 0.4

        val sum = homeOdds * homeMultiplier + drawOdds * drawMultiplier + awayOdds * awayMultiplier
        match!!.odds = listOf(homeOdds * homeMultiplier / sum, drawOdds * drawMultiplier / sum, awayOdds * awayMultiplier / sum)
    }

    private fun sumPoissonOdds(poissonList: MutableList<Double>): Triple<Double, Double, Double>{
        var drawOdds = 0.0
        var homeOdds = 0.0
        var awayOdds = 0.0
        for(i in 1..5){
            for(j in 1..5){
                when {
                    i == j -> drawOdds += poissonList[j - 1] * poissonList[i + 4]
                    i < j -> homeOdds += poissonList[j - 1] * poissonList[i + 4]
                    else -> awayOdds += poissonList[j - 1] * poissonList[i + 4]
                }
            }
        }
        return Triple(homeOdds, drawOdds, awayOdds)
    }

    private fun calculatePoissonOdds(avgGoalsHome: Double, avgGoalsAway: Double,
                                     avgDefendHome: Double, avgDefendAway: Double,
                                     league: League): MutableList<Double>{
        val poissonList = mutableListOf<Double>()
        for(i in 1..10){
            if(i > 5)
                poissonList.add(poisson(avgGoalsAway*avgDefendHome*league.avgGoalsAway, i-6))
            else
                poissonList.add(poisson(avgGoalsHome*avgDefendAway*league.avgGoalsHome, i-1))
        }
        return poissonList
    }

    private fun poisson(lambda: Double, k: Int) : Double {
        return (lambda.pow(k) * E.pow(-lambda)) / factorial(k)
    }

    private fun factorial(num: Int): Long {
        return if (num >= 1)
            num * factorial(num - 1)
        else
            1
    }

    private fun getMultiplier(team : Team, i: Int) : Double{
        var multiplier = 1.0
        if((team.form.length - team.form.replace("W","").length) > 2)
            multiplier += 0.2
        if(match!!.headToHead == "$i")
            multiplier += 0.4
        val standings = LeagueActivity.standingsShort!!
        val teamIDs = LeagueActivity.teamIDs
        if(teamIDs[team.id]!! == 1){
            if(team.points < (standings[teamIDs[team.id]!! + 1].points + 5))
                multiplier += 0.4
        } else if(teamIDs[team.id]!! == 20) {
            if(team.points < (standings[teamIDs[team.id]!! - 1].points - 5))
                multiplier += 0.4
        } else {
            if(team.points < (standings[teamIDs[team.id]!! + 1].points + 5))
                multiplier += 0.4
            else if(team.points < (standings[teamIDs[team.id]!! - 1].points - 5))
                multiplier += 0.4
        }

        return multiplier
    }
}
