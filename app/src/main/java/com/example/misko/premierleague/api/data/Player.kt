package com.example.misko.premierleague.api.data

import java.io.Serializable

data class Player(
        val number: String,
        val player: String
) : Serializable