package com.example.misko.premierleague.local_database.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey

@Entity(
    primaryKeys = ["lineupId", "playerId"],
    foreignKeys = [ForeignKey(
        entity = Lineup::class,
        parentColumns = arrayOf("id"),
        childColumns = arrayOf("lineupId")), ForeignKey(
        entity = Player::class,
        parentColumns = arrayOf("id"),
        childColumns = arrayOf("playerId"))]
)
data class LineupToPlayer(
    @ColumnInfo(index = true) val lineupId: Long,
    @ColumnInfo(index = true) val playerId: Long,
    val isStarting: Int
)