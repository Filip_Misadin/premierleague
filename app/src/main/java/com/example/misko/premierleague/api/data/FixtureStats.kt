package com.example.misko.premierleague.api.data

import com.google.gson.annotations.SerializedName

data class FixtureStats(
        val api: ApiFS
)

data class ApiFS(
        val results: Int,
        val statistics: Statistics
)

data class Statistics(
        @SerializedName("Ball Possession")
        val ballPossession: AwayHome,
        @SerializedName("Blocked Shots")
        val blockedShots: AwayHome,
        @SerializedName("Corner Kicks")
        val cornerKicks: AwayHome,
        @SerializedName("Fouls")
        val fouls: AwayHome,
        @SerializedName("Goalkeeper Saves")
        val goalkeeperSaves: AwayHome,
        @SerializedName("Offsides")
        val offsides: AwayHome,
        @SerializedName("Passes %")
        val passes: AwayHome,
        @SerializedName("Passes accurate")
        val passesAccurate: AwayHome,
        @SerializedName("Red Cards")
        val redCards: AwayHome,
        @SerializedName("Shots insidebox")
        val shotsInsidebox: AwayHome,
        @SerializedName("Shots off Goal")
        val shotsOffGoal: AwayHome,
        @SerializedName("Shots on Goal")
        val shotsOnGoal: AwayHome,
        @SerializedName("Shots outsidebox")
        val shotsOutsidebox: AwayHome,
        @SerializedName("Total passes")
        val totalPasses: AwayHome,
        @SerializedName("Total Shots")
        val totalShots: AwayHome,
        @SerializedName("Yellow Cards")
        val yellowCards: AwayHome
)

data class AwayHome(
        val away: String? = "0",
        val home: String? = "0"
)
