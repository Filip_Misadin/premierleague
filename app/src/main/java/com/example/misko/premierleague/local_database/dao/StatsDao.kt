package com.example.misko.premierleague.local_database.dao

import androidx.room.*
import com.example.misko.premierleague.local_database.entities.Stats

@Dao
interface StatsDao{
    @Query("SELECT * FROM stats")
    fun getAll(): List<Stats>

    @Query("SELECT * FROM stats WHERE id LIKE :id AND lastSeason LIKE 0 ORDER BY home DESC")
    fun findByIdFromThisSeason(id: Long?): List<Stats>

    @Query("SELECT * FROM stats WHERE id LIKE :id AND lastSeason LIKE 1 ORDER BY home DESC")
    fun findByIdFromLastSeason(id: Long?): List<Stats>

    @Query("SELECT id, home, lastSeason, SUM(draw) as draw, SUM(goalsAgainst) as goalsAgainst, SUM(goalsFor) as goalsFor, SUM(lose) as lose, SUM(matchsPlayed) as matchsPlayed, SUM(win) as win FROM stats WHERE lastSeason LIKE 0 AND home LIKE 1 GROUP BY home")
    fun getHomeStats(): Stats

    @Query("SELECT id, home, lastSeason, SUM(draw) as draw, SUM(goalsAgainst) as goalsAgainst, SUM(goalsFor) as goalsFor, SUM(lose) as lose, SUM(matchsPlayed) as matchsPlayed, SUM(win) as win FROM stats WHERE lastSeason LIKE 0 AND home LIKE 0 GROUP BY home")
    fun getAwayStats(): Stats

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(stats: Stats): Long

    @Delete
    fun delete(stats: Stats)

    @Update
    fun updateStats(vararg stats: Stats)
}