package com.example.misko.premierleague.api.data

data class TeamFixtures(
        val api: ApiTF
)

data class ApiTF(
    val fixtures: List<Fixture>,
    val results: Int
)