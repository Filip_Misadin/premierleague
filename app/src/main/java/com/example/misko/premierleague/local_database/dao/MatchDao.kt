package com.example.misko.premierleague.local_database.dao

import androidx.room.*
import com.example.misko.premierleague.local_database.entities.Match

@Dao
interface MatchDao{
    @Query("SELECT * FROM [match]")
    fun getAll(): List<Match>

    @Query("SELECT * FROM [match] WHERE status LIKE 'FT'")
    fun getAllFinished(): List<Match>

    @Query("SELECT * FROM [match] WHERE status LIKE NOT 'FT'")
    fun getAllNotFinished(): List<Match>

    @Query("SELECT * FROM [match] WHERE status LIKE 'FT' ORDER BY date DESC")
    fun getAllFinishedSorted(): List<Match>

    @Query("SELECT * FROM [match] WHERE status LIKE 'TBD' ORDER BY date")
    fun getAllNotFinishedSorted(): List<Match>

    @Query("SELECT * FROM [match] WHERE homeTeamId LIKE :teamId OR awayTeamId LIKE :teamId")
    fun getAllByTeam(teamId: Long?): List<Match>

    @Query("SELECT * FROM [match] ORDER BY date")
    fun getAllSortedByDate(): List<Match>

    @Query("SELECT * FROM [match] WHERE id LIKE :id")
    fun findById(id: Long): Match?

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(vararg match: Match)

    @Delete
    fun delete(match: Match)

    @Update
    fun updateMatch(vararg match: Match)
}