package com.example.misko.premierleague.recyclerview_adapters

import android.content.Intent
import android.graphics.Typeface
import android.view.LayoutInflater
import android.view.ViewGroup
import com.example.misko.premierleague.*
import com.example.misko.premierleague.activities.MatchActivity
import com.example.misko.premierleague.activities.data.MatchShort

import kotlinx.android.synthetic.main.view_match.view.*
import java.text.SimpleDateFormat
import java.util.*


class MatchesAdapter(private var viewlist: MutableList<MatchShort>): androidx.recyclerview.widget.RecyclerView.Adapter<CustomViewHolder>() {
    override fun getItemCount(): Int {
        return viewlist.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val cellForRow = layoutInflater.inflate(R.layout.view_match, parent, false)
        return CustomViewHolder(cellForRow)
    }

    override fun onBindViewHolder(holder: CustomViewHolder, position: Int) {
        val match = viewlist[position]
        val date = formatDate(match.date)
        val hours = formatHours(match.date)

        holder.view.txtMatchDate.text = date
        holder.view.txtMatchHours.text = hours
        holder.view.txtHomeTeam.text = match.homeTeam
        holder.view.txtAwayTeam.text = match.awayTeam
        holder.view.txtGoalsHome.text = match.goalsHome.toString()
        holder.view.txtGoalsAway.text = match.goalsAway.toString()

        holder.view.txtAwayTeam.typeface = Typeface.DEFAULT
        holder.view.txtGoalsAway.typeface = Typeface.DEFAULT
        holder.view.txtHomeTeam.typeface = Typeface.DEFAULT
        holder.view.txtGoalsHome.typeface = Typeface.DEFAULT
        if(match.goalsHome > match.goalsAway) {
            holder.view.txtHomeTeam.typeface = Typeface.DEFAULT_BOLD
            holder.view.txtGoalsHome.typeface = Typeface.DEFAULT_BOLD
        } else if(match.goalsHome != match.goalsAway) {
            holder.view.txtAwayTeam.typeface = Typeface.DEFAULT_BOLD
            holder.view.txtGoalsAway.typeface = Typeface.DEFAULT_BOLD
        }

        holder.view.constraintLayoutMatch.setOnClickListener{
            val intent = Intent(it.context, MatchActivity::class.java)
            intent.putExtra("Match", match)
            it.context.startActivity(intent)
        }
    }

    private fun formatDate(date: String): String{
        val parser = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.getDefault())
        val formatter = SimpleDateFormat("dd.MM.yyyy", Locale.getDefault())
        return formatter.format(parser.parse(date))
    }

    private fun formatHours(date: String): String{
        val parser = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.getDefault())
        val formatter = SimpleDateFormat("HH:mm", Locale.getDefault())
        return formatter.format(parser.parse(date))
    }
}

