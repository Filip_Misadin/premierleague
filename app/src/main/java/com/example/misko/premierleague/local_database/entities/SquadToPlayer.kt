package com.example.misko.premierleague.local_database.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey


@Entity(
    primaryKeys = ["squadId", "playerId"],
    foreignKeys = [ForeignKey(
        entity = Squad::class,
        parentColumns = arrayOf("id"),
        childColumns = arrayOf("squadId")), ForeignKey(
        entity = Player::class,
        parentColumns = arrayOf("id"),
        childColumns = arrayOf("playerId"))]
)
data class SquadToPlayer(
    @ColumnInfo(index = true) val squadId: Long,
    @ColumnInfo(index = true) val playerId: Long
)