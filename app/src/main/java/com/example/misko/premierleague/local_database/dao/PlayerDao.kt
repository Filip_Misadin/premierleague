package com.example.misko.premierleague.local_database.dao

import androidx.room.*
import com.example.misko.premierleague.local_database.entities.Player

@Dao
interface PlayerDao{
    @Query("SELECT * FROM player")
    fun getAll(): List<Player>

    @Query("SELECT * FROM player WHERE id LIKE :id")
    fun findById(id: Long): Player?

    @Query("SELECT * FROM player WHERE teamName LIKE :teamName")
    fun getAllInTeam(teamName: String): List<Player>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(player: Player): Long

    @Delete
    fun delete(player: Player)

    @Update
    fun updatePlayer(vararg player: Player)
}