package com.example.misko.premierleague.activities.data

import com.example.misko.premierleague.api.data.Player
import java.io.Serializable

class Team(var id: Long,
           var name: String,
           var logo: String,
           var form: String,
           var home: Stats,
           var away: Stats,
           var matches: MutableList<MatchShort>?,
           var squad: Squad?,
           var points: Int)

class Squad(var coach: List<String>,
            var players: List<Player>)

class TeamShort(var id: Long,
                var name: String,
                var matchesPlayed: Int,
                var gamesWon: Int,
                var gamesDraw: Int,
                var gamesLost: Int,
                var goalDifference: Int,
                var points: Int) : Serializable



